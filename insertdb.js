var express = require('express')
var app = express()
var bodyParser = require('body-parser')


var MongoClient = require('mongodb').MongoClient
var url = "mongodb://127.0.0.1:27017/newdb"


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'))
app.get('/index.html', function(res, req){ 
    res.sendFile(__dirname + "/" + "index.html")
})


app.post("/value", function (req, res) {
    let reqBody = JSON.stringify(req.body);
    // parsing the request body params need to pass through some middleware for validation
    let doc = JSON.parse(reqBody);
    console.log(doc);
    try{
            MongoClient.connect(url, function(err, db){
                var dbo = db.db("nilanjoydb")
                dbo.collection("testing").insertOne(doc, function(err){
                    db.close()
                    res.status(200).json({message: "success"})
                }) 
            });
         } catch (e){
            console.log(e)
            res.end(JSON.stringify({ message: "fail" }));
        }
    });

 app.listen(8000)  


