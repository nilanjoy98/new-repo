var express = require('express');
var students = require('./students');
var app = express()
app.use(express.json())

app.get('/', function(req, res){
    res.json({message : "API is working"})
});

app.get('/students', function(req, res){
    res.json(students)
});

app.post('/students', function(req, res){
    var user = {
        id : students.length + 1,
        first_name : req.body.first_name,
        last_name : req.body.last_name
    }
    students.push(user)
    res.json(user)
});

//....................PUT...................

app.put('/students/:id', function(req, res){
    let id = req.params.id
    let first_name = req.body.first_name;
    let last_name = req.body.last_name

    let index = students.findIndex( function(student) {
        return (student.id == Number.parseInt(id))
    })

    if (index >= 0)
        {
            let std = students[index]
            std.first_name = first_name
            std.last_name = last_name
            res.json(std) 
        } 

        else
            {
                res.status(404)
            }
    
})

//................ delete.......................

app.delete('/students/:id', function(req, res){
    let id = req.params.id;
    let index = students.findIndex( function(student) {
        return (student.id == Number.parseInt(id))
    })
    if (index >= 0)
        {
            let std = students[index]
            students.splice(index , 1)
            res.json(std) 
        } 

        else
            {
                res.status(404)
            }
    
})





app.listen(8000, () =>
{
    console.log('listining at port 8000');
})  