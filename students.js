var students = [{
    "id": 1,
    "first_name": "Ebeneser",
    "last_name": "Embury"
  }, {
    "id": 2,
    "first_name": "Tobye",
    "last_name": "Priddy"
  }, {
    "id": 3,
    "first_name": "Gray",
    "last_name": "Huniwall"
  }, {
    "id": 4,
    "first_name": "Enriqueta",
    "last_name": "Farquarson"
  }, {
    "id": 5,
    "first_name": "Valentina",
    "last_name": "Cornelius"
  }, {
    "id": 6,
    "first_name": "Gunner",
    "last_name": "Ducker"
  }, {
    "id": 7,
    "first_name": "Allie",
    "last_name": "Rack"
  }, {
    "id": 8,
    "first_name": "Lilian",
    "last_name": "Upfold"
  }, {
    "id": 9,
    "first_name": "Nelson",
    "last_name": "Jumel"
  }, {
    "id": 10,
    "first_name": "Siffre",
    "last_name": "Baird"
  }]

  module.exports = students